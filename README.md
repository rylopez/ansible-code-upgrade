Sequence of Ansible Playbooks I used to upgrade standalone Catalyst switches

1. Code Copy
2. Code Verify
3. Boot statement


The absolute paths provided in the code_copy playbooks are specific to my own Linux host, this will need to be changed to match
your host, unless you create a similar directory structure/path.

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

Run a playbook via 'ansible-playbook -i <INVENTORY FILE> -k <GETPASS METHOD, PROVIDE YOUR TACACS PASSWORD> <PLAYBOOK.YAML>'

See man page/'anisble-playbook -h' for other options. You may need to disable "strict_host_key_checking" as well. 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

IOS Code Copy Ansible Playbook
------------------------------

1. Download target Cisco IOS image, grab the MD5 hash for file verification

2. Install Ansible
	https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html
	#Sudo apt install ansible

3. Install Paramiko
	#pip install paramiko
	***If pip not installed for Python package management, install pip first***
	#sudo apt install python3-pip

4. Pull repository to a local directory
	git clone https://rylopez@bitbucket.org/rylopez/ansible-code-upgrade.git

5. Configure inventory with switches and username (ansible_user) in your environment. Update IOS file name and path, MD5 value, etc.
	If enable secret is required for privilege escalation, see: https://docs.ansible.com/ansible/latest/network/user_guide/platform_ios.html#connections-available

***ALWAYS PERCENTAGE TEST/VALIDATE AGAINST ONE OR SMALL NUMBER OF SWITCHES BEFORE CONFIGURING ALL SWITCHES IN YOUR ENVIRONMENT***
	
6. Validate SSH connectivity from host that will run playbook
	#ssh yourusername@switchipaddress (user1@172.16.1.1)
	if there are any issues, they're likely SSH configuration related, like key exchange algorithms. If so, see: https://docs.ansible.com/ansible/2.9/network/user_guide/network_debug_troubleshooting.html#
	Strict host key checking may also be an issue: https://docs.ansible.com/ansible/latest/inventory_guide/connection_details.html
	For Linux specific config see: https://www.ssh.com/academy/ssh/config

7. Okay, so you're done with setup and have manipulated the inventory and playbook. Time to validate the changes.
	#ansible-playbook -i <your inventory file> -k <playbook>.yaml --syntax-check
	"""#pancake@CSCO-W-PF3B1Q49:~/ansible_ios_config$ ansible-playbook -i lab_test_inventory.yaml -k code_copy.yaml.yaml --syntax-check

	#playbook: code_copy.yaml
	"""
	Expect something similar without any indicated errors if syntax check passes.

	#ansible-playbook -i <your inventory file> -k <playbook>.yaml --check
	
	This will run the playbook, without making any changes, it's predictive. Also tests authentication, SSH, etc.

8. Alright, we're at the real configuration point now:

***ALWAYS PERCENTAGE TEST/VALIDATE AGAINST ONE OR SMALL NUMBER OF SWITCHES BEFORE CONFIGURING ALL SWITCHES IN YOUR ENVIRONMENT***

	#ansible-playbook -i <your inventory file> -k <playbook>.yaml

9.. For the first run through on a single/small batch, please validate that code is there, etc..


ANSIBLE RESOURCES
----------------------

https://docs.ansible.com/ansible/latest/network/getting_started/index.html


https://docs.ansible.com/ansible/latest/collections/index.html


